#!/bin/sh
set -e
# https://learn.adafruit.com/introduction-to-the-beaglebone-black-device-tree/compiling-an-overlay
dtc -O dtb -o msm8998-v2.1-mtp.dtbo -b 0 -@ msm8998-v2.1.dts
mkdtboimg.py cfg_create dtbo.img dtboimg.cfg
