Minimal dtbo partition for Google Pixel 2 XL (google-taimen).

Used for mainline kernel.

This requires dtc and mkdtboimg (from AOSP libufdt) to build.

The pixel 2 XL bootloader is extremely picky about the dtbo partition, it has 'androidboot.dtbo_idx=12' set in the kernel cmdline which led me to read https://source.android.com/devices/architecture/dto/compile and eventually use `mkdtimg dump <stock_dtbo.img>` to discover that the id was set to `0xa90` or `2704` for that overlay, the bootloader will REFUSE TO BOOT unless it finds an overlay with that id in the dtbo partition.

Original script taken from https://gitlab.com/zhuowei/dtbo-google-crosshatch-mainline